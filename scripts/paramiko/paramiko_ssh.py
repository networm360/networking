import getpass
import paramiko

HOST = 'localhost' # or you give the target ip here 
PORT = 22 # port will always be ssh to listen to

def run_cmds(username,password,cmd,hostname=HOST,port=PORT):
    client = parmiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # it automatically adds the SSH Policies for encryption
    client.load_system_host_keys()
    client.connect(hostname,port,username,password,timeout=60) # timeout of 60 seconds
    stdin,stdout,stderr = client.exec_command(cmd)
    print(stdout.read())
    client.dissconnect()

if __name__ == '__main__':
    username = input("Enter Your Username:")
    password = getpass.getpass(prompt="Enter Your Password:")
    cmd = 'ls -l /home' # command to run in Linux distros
    run_cmds(username,password,cmd) # calling the function with the required parameters
